DNS-������
DNS-������
������ DNS ���������

������ 2.0

�����: �������� �����

��������

������ ���������� �������� ����������� DNS-������� � DNS-�������.
���������� ���������� dns-������� � �������� dns-������. �� ����� ������ ������� 4 ���� ���������: IPv4, IPv6, Mail Exchanger, Nameserver.
������������ ��������, debug-mode, tcp.
������ ����� ���������� 2 ���� �������� A � NS.

����������

Python ������ �� ���� 3.4

������

���������� ������: client.py, server.py
�����: tests.py(������ ��� ������� ��������� � �������)
������ DNS ���������: DNSMessage.py
������: DNSClient.py
������: DNSserver.py
���� ��� ������������ ���� � ������� pkl: cache.pkl
���������������� ���� ��� �������-����������: forwarder.conf

���������� ������

�������: ./client.py -h
		./server.py -h
������ �������: ./client.py vk.com

������ ������:
IpV4 : 87.240.165.80
IpV4 : 95.213.11.181
IpV4 : 87.240.129.72
IpV4 : 87.240.129.71

������ �������: ./client.py google.com -s 8.8.8.8 -t AAAA -d -r -tcp -timeout 20

������ ������:
Message ID: 53888
Query/Responce: QUERY
Opcode QUERY
Authoritative Answer: False
TrunCation: False
Recursion Desired: True
Recursion Available: False
Responce Code: No error
Answers 0
Questions: 1
Authority RRs: 0
Additional RRs: 0

Name: google.com
Type: AAAA
Request Class: IN

Message ID: 53888
Query/Responce: RESPONSE
Opcode QUERY
Authoritative Answer: False
TrunCation: False
Recursion Desired: True
Recursion Available: True
Responce Code: No error
Answers 1
Questions: 1
Authority RRs: 0
Additional RRs: 0

Name: google.com
Type: AAAA
Request Class: IN

Name: google.com
Type: AAAA
Request Type: IN
Time To Live: 299
Resource Data Length: 16
Resource Data Type: AAAA

IpV6 : 2a00:1450:4010:c08::::8b

������ �������: ./server.py -timeout 20 -conf 'another.conf' -cache 'another_cache.pkl' -debug