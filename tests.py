import unittest
from DNSClient import DNSClient
from DNSMessage import *


class TestClient(unittest.TestCase):
    def setUp(self):
        self.client = DNSClient()
        query = b'\xee\x82\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00\x02\x76\x6b\x03\x63\x6f\x6d\x00\x00\x01\x00\x01'
        '''
        Transaction ID: 0xee82
        Flags: 0x0100 Standard query
            0... .... .... .... = Response: Message is a query
            .000 0... .... .... = Opcode: Standard query (0)
            .... ..0. .... .... = Truncated: Message is not truncated
            .... ...1 .... .... = Recursion desired: Do query recursively
            .... .... .0.. .... = Z: reserved (0)
            .... .... ...0 .... = Non-authenticated data: Unacceptable
        Questions: 1
        Answer RRs: 0
        Authority RRs: 0
        Additional RRs: 0
        Queries
            vk.com: type A, class IN
                Name: vk.com
                [Name Length: 6]
                [Label Count: 2]
                Type: A (Host Address) (1)
                Class: IN (0x0001)'''
        self.header = query[2:12]
        self.question = query[12:]
        message = b'\xee\x82\x81\x80\x00\x01\x00\x04\x00\x00\x00' \
                       b'\x00\x02\x76\x6b\x03\x63\x6f\x6d\x00\x00\x01' \
                       b'\x00\x01\xc0\x0c\x00\x01\x00\x01\x00\x00\x00' \
                       b'\xd5\x00\x04\x57\xf0\x81\x48\xc0\x0c\x00\x01' \
                       b'\x00\x01\x00\x00\x00\xd5\x00\x04\x57\xf0\xb6' \
                       b'\xe0\xc0\x0c\x00\x01\x00\x01\x00\x00\x00\xd5' \
                       b'\x00\x04\x57\xf0\x81\x47\xc0\x0c\x00\x01\x00' \
                       b'\x01\x00\x00\x00\xd5\x00\x04\x5f\xd5\x0b\xb5'
        '''
        Transaction ID: 0xee82
        Flags: 0x8180 Standard query response, No error
            1... .... .... .... = Response: Message is a response
            .000 0... .... .... = Opcode: Standard query (0)
            .... .0.. .... .... = Authoritative: Server is not an authority for domain
            .... ..0. .... .... = Truncated: Message is not truncated
            .... ...1 .... .... = Recursion desired: Do query recursively
            .... .... 1... .... = Recursion available: Server can do recursive queries
            .... .... .0.. .... = Z: reserved (0)
            .... .... ..0. .... = Answer authenticated: Answer/authority portion was not authenticated by the server
            .... .... ...0 .... = Non-authenticated data: Unacceptable
            .... .... .... 0000 = Reply code: No error (0)
        Questions: 1
        Answer RRs: 4
        Authority RRs: 0
        Additional RRs: 0
        Queries
            vk.com: type A, class IN
                Name: vk.com
                [Name Length: 6]
                [Label Count: 2]
                Type: A (Host Address) (1)
                Class: IN (0x0001)
        Answers
            vk.com: type A, class IN, addr 87.240.129.72
                Name: vk.com
                Type: A (Host Address) (1)
                Class: IN (0x0001)
                Time to live: 213
                Data length: 4
                Address: 87.240.129.72
            vk.com: type A, class IN, addr 87.240.182.224
                Name: vk.com
                Type: A (Host Address) (1)
                Class: IN (0x0001)
                Time to live: 213
                Data length: 4
                Address: 87.240.182.224
            vk.com: type A, class IN, addr 87.240.129.71
                Name: vk.com
                Type: A (Host Address) (1)
                Class: IN (0x0001)
                Time to live: 213
                Data length: 4
                Address: 87.240.129.71
            vk.com: type A, class IN, addr 95.213.11.181
                Name: vk.com
                Type: A (Host Address) (1)
                Class: IN (0x0001)
                Time to live: 213
                Data length: 4
                Address: 95.213.11.181
        '''
        self.decoded_message = MessageFormat()
        self.decoded_message.decode(message, False)
        self.answers = {
                        'A': b'\x02\x76\x6b\x03\x63\x6f\x6d\x00\x00\x01'
                             b'\x00\x01\x00\x00\x00\xd5\x00\x04\x57\xf0\x81\x48',
                        'MX': b'\x02\x76\x6b\x03\x63\x6f\x6d\x00\x00\x0f\x00\x01\x00\x00\x01'
                              b'\xe8\x00\x07\x00\x00\x02\x6d\x78\x02\x76\x6b\x03\x63\x6f\x6d\x00',
                        'AAAA': b'\x06\x67\x6f\x6f\x67\x6c\x65\x03\x63\x6f\x6d\x00\x00\x1c\x00\x01'
                                b'\x00\x00\x01\x2b\x00\x10\x2a\x00\x14\x50\x40\x0f\x08\x08\x00\x00\x00\x00\x00\x00\x20\x0e',
                        'NS': b'\x02\x76\x6b\x03\x63\x6f\x6d\x00\x00\x02\x00\x01\x00\x00\x02\x66\x00\x12\x03\x6e\x73\x31'
                              b'\x09\x76\x6b\x6f\x6e\x74\x61\x6b\x74\x65\x02\x72\x75\x00'}

    def tearDown(self):
        self.client.socket.close()

    def test_UDP_default(self):
        self.assertEqual(self.client.tcp_needed, False)

    def test_TCP_init(self):
        self.client.socket.close()
        self.client = DNSClient(tcp_needed=True)
        self.assertEqual(self.client.tcp_needed, True)

    def test_header_encode(self):
        header = Header()
        header.set(recursion=True)
        binary_header = header.encode()
        self.assertEqual(binary_header[2:], self.header)

    def test_question_encode(self):
        question = Question()
        question.set('vk.com', 'A')
        binary_question = question.encode()
        self.assertEqual(binary_question, self.question)

    def test_header_decode(self):
        header = self.decoded_message.header
        self.assertEqual(header.qr, 1)
        self.assertEqual(header.opcode, 0)
        self.assertEqual(header.aa, 0)
        self.assertEqual(header.tc, 0)
        self.assertEqual(header.rd, 1)
        self.assertEqual(header.ra, 1)
        self.assertEqual(header.rcode, 0)
        self.assertEqual(header.questions_count, 1)
        self.assertEqual(header.answers_count, 4)
        self.assertEqual(header.authority_count, 0)
        self.assertEqual(header.additional_count, 0)

    def test_question_decode(self):
        questions = self.decoded_message.questions
        for i in range(self.decoded_message.header.questions_count):
            self.assertEqual(questions[i].name, 'vk.com')
            self.assertEqual(questions[i].type, 1)
            self.assertEqual(questions[i].request_class, 1)

    def test_resource_records_decode(self):
        answers = self.decoded_message.answers
        for i in range(self.decoded_message.header.answers_count):
            self.assertEqual(answers[i].name, 'vk.com')
            self.assertEqual(answers[i].type, 1)
            self.assertEqual(answers[i].request_class, 1)
            self.assertEqual(answers[i].ttl, 213)
            self.assertEqual(type(answers[i].resource_data), AResourceData)
            self.assertEqual(answers[i].resource_data_length, 4)

    def test_different_rrs_decode(self):
        rr = ResourceRecord()
        rr.decode(self.answers['A'], 0)
        self.assertEqual(type(rr.resource_data), AResourceData)
        rr.decode(self.answers['AAAA'], 0)
        self.assertEqual(type(rr.resource_data), AAAAResourceData)
        rr.decode(self.answers['NS'], 0)
        self.assertEqual(type(rr.resource_data), NSResourceData)
        rr.decode(self.answers['MX'], 0)
        self.assertEqual(type(rr.resource_data), MXResourceData)


if __name__ == "__main__":
    unittest.main()
