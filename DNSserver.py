import socket, datetime, pickle, os
from DNSMessage import MessageFormat

class DNSserver:
    def __init__(self, timeout, config, debug, cache_file):
        self.cache = {}
        self.timeout = timeout
        self.debug = debug
        self.cache_file = cache_file
        with open(config, 'r') as file:
            self.forwarder = file.readline()

    def initialize_cache(self):
        if os.stat(self.cache_file).st_size is not 0:
            with open(self.cache_file, 'rb') as input:
                count = pickle.load(input)
                i = 0
                while i < count:
                    key = pickle.load(input)
                    time = pickle.load(input)
                    if time > datetime.datetime.utcnow():
                        self.cache[key] = time
                    i+=1

    def serialize_cache(self):
        with open(self.cache_file, 'wb') as output:
            count = len(self.cache)
            pickle.dump(count, output, pickle.HIGHEST_PROTOCOL)
            for key in self.cache.keys():
                pickle.dump(key, output, pickle.HIGHEST_PROTOCOL)
                pickle.dump(self.cache[key], output, pickle.HIGHEST_PROTOCOL)

    def add_to_cache(self, data):
        message = MessageFormat()
        message.decode(data, self.debug)
        for answer in message.answers:
            answer.resource_data_length = answer.resource_data.len
            if answer.ttl > 0:
                self.cache[answer] = datetime.datetime.utcnow() + datetime.timedelta(seconds=answer.ttl)
        for auth in message.authority_RRs:
            auth.resource_data_length = auth.resource_data.len
            if auth.ttl > 0:
                self.cache[auth] = datetime.datetime.utcnow() + datetime.timedelta(seconds=auth.ttl)
        for ad in message.additional_RRs:
            ad.resource_data_length = ad.resource_data.len
            if ad.ttl>0:
                self.cache[ad] = datetime.datetime.utcnow() + datetime.timedelta(seconds=ad.ttl)

    def find_in_cache(self, data):
        message = MessageFormat()
        message.decode(data, self.debug)
        message.header.qr = 1
        binary_mes = b''
        to_delete = []
        for item in self.cache.keys():
            if item.name == message.questions[0].name and item.type == message.questions[0].type:
                if self.cache[item] > datetime.datetime.utcnow():
                    message.answers.append(item)
                    message.header.answers_count += 1
                    binary_mes = message.header.encode() + message.questions[0].encode()
                    for answer in message.answers:
                        binary_mes += answer.encode()
                else:
                    to_delete.append(item)
        for item in to_delete:
            self.cache.pop(item)
        return binary_mes, len(message.answers)

    def run(self):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as server:
            server.bind(('localhost', 53))
            server.settimeout(self.timeout)
            while True:
                print('Waiting...')
                try:
                    data, address = server.recvfrom(512)
                    response, len = self.find_in_cache(data)
                    if len==0:
                        response = self.forward(data)
                        self.add_to_cache(response)
                    server.sendto(response, address)
                except socket.timeout:
                    print('Socket timeout passed.({0} seconds)'.format(server.timeout))
                    self.serialize_cache()
                    return

    def forward(self, data):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as client_side:
            client_side.sendto(data, (self.forwarder, 53))
            client_side.settimeout(self.timeout)
            try:
                response = client_side.recv(512)
            except socket.timeout:
                print('Socket timeout passed.({0} seconds)'.format(self.timeout))
                self.serialize_cache()
                return
            else:
                return response
