import socket
from struct import pack
from DNSMessage import MessageFormat, response_code_names


class DNSClient:
    def __init__(self, server='8.8.8.8', tcp_needed=False, timeout=30):
        self.tcp_needed = tcp_needed
        self.server = server
        self.timeout = timeout

    def send_query(self, host, query_type='A', recursion=False, debug=False):
        message = MessageFormat()
        query = message.encode(host, recursion, query_type, debug)
        query = pack('!H',len(query))+query if self.tcp_needed else query
        with socket.socket(socket.AF_INET,socket.SOCK_STREAM if self.tcp_needed else socket.SOCK_DGRAM) as client:
            client.settimeout(self.timeout)
            client.connect((self.server, 53))
            client.send(query)
            try:
                response = client.recv(512)
            except socket.timeout:
                print('Time Out: {0}'.format(self.server))
                exit(0)
            else:
                response = response[2:] if self.tcp_needed else response
                message = MessageFormat()
                message.decode(response, debug)
                if message.header.rcode is not 0:
                    print(f'Error code was recieved: {response_code_names[message.header.rcode]}')

                if len(message.answers) > 0:
                    print('answers:')
                    for answer in message.answers:
                        print(answer.resource_data)

                if len(message.authority_RRs) > 0:
                    print('authority rrs:')
                    for rr in message.authority_RRs:
                        print(rr.resource_data)

                if len(message.additional_RRs) > 0:
                    print('additional rrs:')
                    for rr in message.additional_RRs:
                        print(rr.resource_data)
