from struct import pack, unpack
import random
import binascii


query_type_names = {'A': 1, 'NS': 2, 'MX': 15, 'AAAA': 28}
type_names = {1: 'A', 2: 'NS', 15: 'MX', 28: 'AAAA'}
opcodes = { 0:'QUERY', 1:'IQUERY', 2:'STATUS' }
query_class_names = { 1:'IN' }
message_types = { 0:'QUERY', 1:'RESPONSE' }
response_code_names = { 0:'No error', 1:'Format error',
2:'Server failure', 3:'Name error', 4:'Not implemented', 5:'Refused' }


def decode_name(message, position):
    index = position
    result = ''
    position = 0
    while message[index] != 0:
        value = message[index]
        if (value>>6) == 3:
            next = unpack('!H',message[index:index+2])[0]
            if position is 0:
                position = index + 2
            index = next ^ (3<<14)
        else:
            result += message[index + 1:index + 1 + value].decode('utf-8') + '.'
            index += value + 1
    if position is 0:
        position = index + 1
    result = result[:-1]
    return result, position


def encode_name(host_name):
    name = host_name.split('.')
    packet = b''
    for domain_name in name:
        packet += pack("!B", len(domain_name))
        for byte in domain_name:
            packet += pack("!c", byte.encode('utf-8'))
    packet += b'\x00'
    return packet

class MessageFormat:
    def __init__(self):
        self.header = None
        self.question = None
        self.questions = None
        self.answers = None
        self.authority_RRs = None
        self.additional_RRs = None


    def encode(self, host_name, recursion, query_type, debug):
        message = b''
        self.header = Header()
        self.header.set(recursion)
        message += self.header.encode()
        self.question = Question()
        self.question.set(host_name, query_type)
        message += self.question.encode()
        if debug:
            print(self.header)
            print(self.question)
        return message

    def decode(self, message, debug):
        self.header = Header()
        position = self.header.decode(message)
        if debug: print(self.header)
        self.questions = []
        self.answers = []
        self.authority_RRs = []
        self.additional_RRs = []
        for i in range(self.header.questions_count):
            self.questions.append(Question())
            position = self.questions[i].decode(message, position)
            if debug: print(self.questions[i])
            for j in range(self.header.answers_count):
                self.answers.append(ResourceRecord())
                position = self.answers[j].decode(message, position)
                if debug: print(self.answers[i])
            for j in range(self.header.authority_count):
                self.authority_RRs.append(ResourceRecord())
                position = self.authority_RRs[j].decode(message, position)
                if debug: print(self.authority_RRs[i])
            for j in range(self.header.additional_count):
                self.additional_RRs.append(ResourceRecord())
                position = self.additional_RRs[j].decode(message, position)
                if debug: print(self.additional_RRs[i])


class Header:
    def __init__(self):
        self.messageID = None
        self.qr = None
        self.opcode = None
        self.aa = None
        self.tc = None
        self.rd = None
        self.ra = None
        self.rcode = None
        self.questions_count = None
        self.answers_count = None
        self.authority_count = None
        self.additional_count = None

    def decode(self, message):
        self.messageID = unpack("!H", message[0:2])[0]
        meta = unpack("!H", message[2:4])[0]
        self.rcode = (meta & 15)
        meta >>= 7
        self.ra = (meta & 1)
        meta >>= 1
        self.rd = (meta & 1)
        meta >>= 1
        self.tc = (meta & 1)
        meta >>= 1
        self.aa = (meta & 1)
        meta >>= 1
        self.opcode = (meta & 15)
        meta >>= 4
        self.qr = meta
        self.questions_count = unpack("!H", message[4:6])[0]
        self.answers_count = unpack("!H", message[6:8])[0]
        self.authority_count = unpack("!H", message[8:10])[0]
        self.additional_count = unpack("!H", message[10:12])[0]
        return 12

    def set(self, recursion):
        self.messageID = random.randint(0, 2**16)
        self.qr = 0
        self.opcode = 0
        self.aa = 0
        self.tc = 0
        self.rd = int(recursion)
        self.ra = 0
        self.rcode = 0
        self.questions_count = 1
        self.answers_count = 0
        self.authority_count = 0
        self.additional_count = 0

    def encode(self):
        packet = pack("!H", self.messageID)
        meta = 0
        meta |= self.qr
        meta <<= 1
        meta |= self.opcode
        meta <<= 4
        meta |= self.aa
        meta <<= 1
        meta |= self.tc
        meta <<= 1
        meta |= self.rd
        meta <<= 1
        meta |= self.ra
        meta <<= 7
        meta |= self.rcode
        packet += pack("!H", meta)
        packet += pack("!H", self.questions_count)
        packet += pack("!H", self.answers_count)
        packet += pack("!H", self.authority_count)
        packet += pack("!H", self.additional_count)
        return packet

    def __str__(self):
        print ('Message ID: {}\n' \
               'Query/response: {}\n' \
               'Opcode: {}\n' \
               'Authoritative Answer: {}\n' \
               'TrunCation: {}\n' \
               'Recursion Desired: {}\n' \
               'Recursion Available: {}\n' \
               'response Code: {}\n' \
               'Answers {}\n' \
               'Questions: {}\n' \
               'Authority RRs: {}\n' \
               'Additional RRs: {}'.format(self.messageID,
                         message_types[self.qr],
                         opcodes[self.opcode],
                         bool(self.aa),
                         bool(self.tc),
                         bool(self.rd),
                         bool(self.ra),
                         response_code_names[self.rcode],
                         self.answers_count,
                         self.questions_count,
                         self.authority_count,
                         self.additional_count))
        return ''


class Question:
    def __init__(self):
        self.name = None
        self.type = None
        self.request_class = None

    def decode(self, message, position):
        self.name, position = decode_name(message, position)
        self.type = unpack("!H", message[position:position + 2])[0]
        self.request_class = unpack("!H", message[position + 2:position + 4])[0]
        return position + 4

    def set(self, name, query_type):
        self.name = name
        self.type = query_type_names[query_type]
        self.request_class = 1

    def encode(self):
        packet = encode_name(self.name)
        packet += pack("!H", self.type)
        packet += pack("!H", self.request_class)
        return packet

    def __str__(self):
        print('Name: {}\n'
              'Type: {}\n'
              'Request Class: {}'.format(self.name, type_names[self.type], query_class_names[self.request_class]))
        return ''


class ResourceRecord:
    def __init__(self):
        self.name = None
        self.type = None
        self.request_class = None
        self.ttl = None
        self.resource_data = None
        self.resource_data_length = None

    def encode(self):
        packet = encode_name(self.name)
        packet += pack("!H", self.type)
        packet += pack("!H", self.request_class)
        packet += pack("!I", self.ttl)
        packet += pack("!H", self.resource_data_length)
        packet += self.encode_resource_data()
        return packet

    def decode(self, message, position):
        self.name, position = decode_name(message, position)
        self.type = unpack("!H", message[position:position + 2])[0]
        position += 2
        self.request_class = unpack("!H", message[position:position + 2])[0]
        position += 2
        self.ttl = unpack("!I", message[position: position + 4])[0]
        position += 4
        self.resource_data_length = unpack("!H", message[position:position + 2])[0]
        position += 2
        self.set_resource_data(message, position)
        return position + self.resource_data_length

    def set_resource_data(self, message, position):
        resource_data_names = {1: AResourceData, 15: MXResourceData, 28: AAAAResourceData, 2: NSResourceData}
        data = message[position: position + self.resource_data_length]
        if self.type not in type_names:
            self.resource_data = NotImplementedRD(self.type)
        elif self.type == 1 or self.type == 28:
            self.resource_data = resource_data_names[self.type](data, self.name)
        else:
            self.resource_data = resource_data_names[self.type](message, position, self.name)

    def encode_resource_data(self):
        return self.resource_data.encode()

    def __str__(self):
        print('Name: {}\n'
              'Type: {}\n'
              'Request Type: {}\n'
              'Time To Live: {}\n'
              'Resource Data Length: {}\n'
              'Resource Data Type: {}'
              .format(self.name,
                    type_names[self.type] if self.type in type_names else self.type,
                    query_class_names[self.request_class],
                    self.ttl,
                    self.resource_data_length,
                    type_names[self.resource_data.type] if self.type in type_names else self.type))
        return ''

class NotImplementedRD:
    def __init__(self, type):
        self.type = type

    def __str__(self):
        return f"The RD is not realised for now, sorry, its type value is {self.type}"

class NSResourceData:
    def __init__(self, message, position, name):
        self.type = 2
        self.host_name = name
        self.name = decode_name(message, position)[0]
        self.len = len(self.name.split('.')[:-1])+len(self.name)

    def __str__(self):
        return "DNS server for {}: {}".format(self.host_name, self.name)

    def encode(self):
        return encode_name(self.name)

class AAAAResourceData:
    def __init__(self, data, name):
        self.type = 28
        self.ip = ''
        self.host_name = name
        hexlified = binascii.hexlify(data).decode()
        for i in range(8):
            part = hexlified[i*4:i*4+4]
            for j in range(4):
                if part[j] != '0':
                    part = part[j:]
                    break
                if j == 3:
                    part = ''
            self.ip += part + ':'
        self.ip = self.ip[:-1]

    def __str__(self):
        return "IpV6 for {}: {}".format(self.host_name, self.ip)


class AResourceData:
    def __init__(self, data, name):
        self.type = 1
        self.host_name = name
        self.ip = unpack("BBBB", data)
        self.len = len(self.ip)

    def __str__(self):
        return "IpV4 for {}: {}.{}.{}.{}".format(self.host_name, self.ip[0], self.ip[1], self.ip[2], self.ip[3])

    def encode(self):
        return pack("BBBB", self.ip[0],self.ip[1], self.ip[2], self.ip[3])

class MXResourceData:
    def __init__(self, message, position, name):
        self.type = 15
        self.host_name = name
        self.preference = unpack("!H", message[position:position+2])[0]
        position += 2
        self.mail_exchanger = decode_name(message, position)[0]

    def __str__(self):
        return "preference: {}\nMX for {}: {}".format(self.preference, self.host_name, self.mail_exchanger)
