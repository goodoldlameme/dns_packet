from DNSserver import DNSserver
import argparse

def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='''Simple caching DNS server. 
                        Keys: -timeout timeout value, 
                        -conf configuration file with forwarder dns server address,
                        -cache cache file
                        -debug debug mode'''
    )
    parser.add_argument('-timeout', help='timeout value', default=30)
    parser.add_argument('-conf', help='configuration file with forwarder dns server address', default='forwarder.conf')
    parser.add_argument('-cache', help='cache file', default='cache.pkl')
    parser.add_argument('-debug', help='enable debug mode', nargs='?', type=bool, const=True, default=False)
    args = parser.parse_args()

    server = DNSserver(args.timeout, args.conf, args.debug, args.cache)
    server.initialize_cache()
    server.run()

if __name__ == '__main__':
    main()