from DNSClient import DNSClient
import argparse


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='''Simple DNS client. Format: domain name with keys
                        -t type, -r recursion, -tcp tcp used, -s server, -d debug mode'''
    )
    parser.add_argument('name', help='dns name')
    parser.add_argument('-timeout', help='timeout value', default=30)
    parser.add_argument('-t', '--type', help='type of query', default='A')
    parser.add_argument('-r', '--recursion', help='enable recursion', nargs='?', type=bool, const=True, default=False)
    parser.add_argument('-tcp', help='TCP needed', nargs='?', type=bool, const=True, default=False)
    parser.add_argument('-s', '--server', help='server', default='8.8.8.8')
    parser.add_argument('-d', '--debug', help='enable debug mode', nargs='?', type=bool, const=True, default=False)
    args = parser.parse_args()

    client = DNSClient(server=args.server, tcp_needed=args.tcp, timeout=args.timeout)

    query_type = ['A', 'NS', 'MX', 'AAAA', 'SOA']

    if query_type.__contains__(args.type):
        client.send_query(args.name, args.type, args.recursion, args.debug)
    else:
        print("The type is not supported")


if __name__ == "__main__":
    main()

